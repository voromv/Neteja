function preDialog() {
    sleep 2s
    echo
    echo "************************************************************************"
    echo "**                                                                    **"
    echo "**                           NETEJA                                   **"  
    echo "**                                                                    **"
    echo "************************************************************************"
    echo
    echo "Para usar Neteja necesitamos de la utilidad dialog."
    echo "En caso de que no la tengas instalada, procederemos a instalar la misma."
    echo "Ingrese su contraseña de superusuario para continuar."
    echo
    echo
}

function presentacion() {
    dialog --title $titulo --msgbox " 
    
            HOLA!!
            Este es el Script de Voro MV, para limpiar el sistema.
           
            Se llama NETEJA
            y está en su 4ª versión
    
            Con él, vamos a actualizar el sistema.
            También Flatpak y Snap, si los tienes instalados.
            Finalmente eliminaremos toda la basurilla. ¿Ok?
               
          " 0 0
}

function ayuda() {
    echo
    echo Neteja
    echo
    echo ./neteja.sh [-v][--version] [-h][--help]
    echo
    echo [-v][--version]: Argumento opcional que indica la versión del script.
    echo
    echo [-h][--help]: Argumento opcional que muestra la ayuda del script.
    echo
    echo Descripción Script:
    echo
    echo Con él, vamos a actualizar el sistema.
    echo También Flatpak y Snap, si los tienes instalados.
    echo Finalmente eliminaremos toda la basurilla.
    echo
    echo Nota:
    echo
    echo En el proceso, has tenido que poner tu contraseña de usuario.
    echo
}

function dirTemporales() {
    dialog --title $titulo --msgbox "Vamos a borrar los archivos temporales" 0 0
    dialog --title $titulo --infobox "Borramos directorios temporales" 0 0

    sudo rm -vfr /tmp/* >/dev/null 2>&1
    sudo rm -vfr /var/tmp/* >/dev/null 2>&1
}

function papelera() {
    dialog --title $titulo --yesno "
      Vamos a vaciar la Papelera.
      
      Ten en cuenta que no podrás recuperar nada de lo que había en ella.
      
      ¿Estas segur@ de querer borrar el contenido?
      
      " 0 0
    if [ $? == 0 ]; then
        dialog --title $titulo --infobox "Borrando la papelera..." 0 0
        sudo rm -rf ~/.local/share/Trash/* >/dev/null 2>&1
    fi
}

function despedida() {
    dialog --title $titulo --msgbox " 
    
     Ya hemos terminado.
      
     Ya tienes tu sistema limpio.
    
     Este Script es público, está creado por la comunidad de Voro MV, y disfruta de las 4 libertades:
     ~ La libertad de ejecutar el software como te plazca y con cualquier objetivo.
     ~ La libertad de estudiar como funciona el programa y cambiarlo a tu gusto.
     ~ La libertad de poder redistribuir copias del programa a los demás.
     ~ La libertad de poder distribuir también tus mejoras al programa original." 0 0
    clear
}
