function fPacman() {
  dialog --title $titulo --msgbox " 
     Actualizamos el sistema.
    
     También Flatpak y Snap, si los tienes instalados. 
     " 0 0

  dialog --title $titulo --infobox "Actualizando el sistema..." 0 0
  sudo pacman -Syyu --noconfirm >/dev/null 2>&1
  aux=$(pacman -Qi flatpak 2>&1)
  [[ "$aux" == "error: package 'flatpak' was not found" ]] && dialog --title $titulo --msgbox "Flatpak no detectado" 0 0 || dialog --title $titulo --infobox "Actualizando Flatpak..." 0 0 && sudo flatpak upgrade -y >/dev/null 2>&1
  aux=$(pacman -Qi snapd 2>&1)
  [[ "$aux" == "error: package 'snapd' was not found" ]] && dialog --title $titulo --msgbox "Snap no detectado" 0 0 || dialog --title $titulo --infobox "Actualizando Snap..." 0 0 && sudo snap refresh >/dev/null 2>&1
}

function fPacmanCache {
  dialog --title $titulo --msgbox "
    También borramos versiones antiguas de programas, 
    Kernel, cachés, repositorios no usados, 
    paquetes huerfanos...
    
    " 0 0

  dialog --title $titulo --infobox "Borrando cachés, etc..." 0 0 &&
    sudo pacman -Scc --noconfirm >/dev/null 2>&1
  sudo pacman -Rn $(pacman -Qqdt) --noconfirm >/dev/null 2>&1

}